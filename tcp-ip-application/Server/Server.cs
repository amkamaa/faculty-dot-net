﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Server
{
    internal class Server
    {
        private static readonly TcpListener Listener = new TcpListener(IPAddress.Any, 2105);
        private static NetworkStream _stream;

        private static void Main(string[] args)
        {
            Console.Title = "Server";
            ServerSetup();
        }

        private static void ServerSetup()
        {
            Console.WriteLine("Server is running...");

            while (true)
            {
                Listener.Start(1);
                var socket = Listener.AcceptSocket();
                _stream = new NetworkStream(socket);

                try
                {
                    ProcessAndSendResponse(ReceivedMessage());
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        private static void ProcessAndSendResponse(string receivedMessage)
        {
            try
            {
                var fullPath = Path.GetFullPath(receivedMessage);
                var attr = File.GetAttributes(fullPath);
                var response = attr.HasFlag(FileAttributes.Directory) ? GetFiles(fullPath) : File.ReadAllText(fullPath);

                if (response == "")
                    response = "Empty result. Try again!";
                SendMessage(response);
            }
            catch (Exception)
            {
                SendMessage($"\"{receivedMessage}\" is not a valid path!");
            }
        }

        private static string GetFiles(string fullPath)
        {
            var response = "";
            var files = Directory.GetFiles(fullPath);

            foreach (var file in files) response += Path.GetFileName(file) + "\n";

            return response.Substring(0, response.Length - 1);
        }

        private static string ReceivedMessage()
        {
            var message = "";

            if (!_stream.CanRead)
                return "";

            do
            {
                var buffer = new byte[1024];
                var numberOfBytesRead = _stream.Read(buffer, 0, buffer.Length);

                message += Encoding.ASCII.GetString(buffer, 0, numberOfBytesRead);
            } while (_stream.DataAvailable);

            return message;
        }

        private static void SendMessage(string message)
        {
            var bytes = Encoding.UTF8.GetBytes(message);
            _stream.Write(bytes, 0, bytes.Length);
        }
    }
}