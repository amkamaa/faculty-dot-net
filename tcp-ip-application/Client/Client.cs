﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Client
{
    internal class Client
    {
        private const int MAX_ATTEMPTS = 10;
        private static TcpClient _client;

        private static void Main(string[] args)
        {
            Console.Title = "Client";
            ClientSetup();
            Console.ReadLine();
        }

        private static void ClientSetup()
        {
            LoopConnect();

            var stream = _client.GetStream();

            Console.WriteLine("Connected to server!");
            Console.Write("Enter a message: ");

            var message = Console.ReadLine();
            SendMessage(stream, message);
            Console.WriteLine("Message was sent!\n");
            message = ReceiveMessage(stream);

            if (message != "") Console.WriteLine($"Server response is: \n\"{message}\"");

            Console.WriteLine("\nPress enter to exit!");
        }

        private static void LoopConnect()
        {
            var attempts = 0;
            while (_client == null && attempts < MAX_ATTEMPTS)
                try
                {
                    attempts++;
                    _client = new TcpClient("127.0.0.1", 2105);
                    Console.Clear();
                }
                catch (Exception)
                {
                    Console.Clear();
                    Console.WriteLine($"Try {attempts} to connect to server...");
                    Thread.Sleep(500);
                }

            if (attempts != MAX_ATTEMPTS) return;

            Console.WriteLine("Maximum number of attempts was reached. The server does not respond.");
            Thread.Sleep(5000);
            Environment.Exit(1);
        }

        private static void SendMessage(NetworkStream stream, string message)
        {
            var buffer = Encoding.ASCII.GetBytes(message);
            stream.Write(buffer, 0, buffer.Length);
        }

        private static string ReceiveMessage(NetworkStream stream)
        {
            var message = "";

            if (!stream.CanRead)
                return "";

            do
            {
                var buffer = new byte[1024];
                var numberOfBytesRead = stream.Read(buffer, 0, buffer.Length);

                message += Encoding.ASCII.GetString(buffer, 0, numberOfBytesRead);
            } while (stream.DataAvailable);

            return message;
        }
    }
}