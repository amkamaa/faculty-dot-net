﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Net;

namespace WebCrawler
{
    class PageCrawler
    {
        private Uri Uri;
        private HtmlDocument Html = new HtmlDocument();

        public PageCrawler(Uri uri)
        {
            Uri = uri;
            FetchHtml();
        }

        private void FetchHtml()
        {
            var client = new WebClient();
            Html.LoadHtml(client.DownloadString(Uri));
        }

        public List<Product> GetProducts()
        {
            var products = new List<Product>();
            var htmlProducts = ExtractHtmlProducts();

            if (htmlProducts != null)
            {
                foreach (var htmlNode in htmlProducts)
                {
                    products.Add(new Product()
                    {
                        Name = ExtractProductName(htmlNode),
                        Price = Single.Parse(ExtractProductPrice(htmlNode))
                    });
                }
            }
            
            return products;
        }

        public List<Uri> GetUris()
        {
            var uris = new List<Uri>();

            foreach(var aTag in Html.DocumentNode.SelectNodes("//a"))
            {
                try
                {
                    Uri currentUri = new Uri(aTag.Attributes["href"].Value);

                    if (!uris.Contains(currentUri) && currentUri.ToString().Contains(Uri.Host))
                    {
                        uris.Add(currentUri);
                    }
                }
                catch (Exception)
                {
                    // ignored
                }
            }

            return uris;
        }

        private HtmlNodeCollection ExtractHtmlProducts()
        {
            return Html.DocumentNode.SelectNodes("//div[@class='product-box']");
        }

        private string ExtractProductName(HtmlNode product)
        {
            return product.SelectSingleNode(".//div[@class='pb-name']/a").InnerText.Trim();
        }

        private string ExtractProductPrice(HtmlNode product)
        {
            return product.SelectSingleNode(".//div[@class='pb-price']/p[@class='price']").InnerText.Replace("RON", "").Trim();
        }
    }
}
