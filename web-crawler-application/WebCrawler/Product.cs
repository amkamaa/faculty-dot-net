﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCrawler
{
    class Product
    {
        public string Name;
        public float Price;

        public override string ToString()
        {
            return "Name: " + Name + ", Price: " + Price + " lei";
        }
    }
}
