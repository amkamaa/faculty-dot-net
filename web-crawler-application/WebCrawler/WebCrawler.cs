﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WebCrawler
{
    class WebCrawler
    {
        private Uri _rootUri;
        private List<Uri> _visitedUris;
        private SortedList _topProducts;
        private int _nrProducts;


        public WebCrawler(Uri uri, int nrProducts)
        {
            _rootUri = uri;
            _nrProducts = nrProducts;
            _visitedUris = new List<Uri>();
            _topProducts = new SortedList();
        }

        public SortedList TopProducts
        {
            get { return _topProducts; }
        }

        public List<Uri> VisitedUris
        {
            get { return _visitedUris; }
        }

        public void CrawlPages()
        {
            RecursiveCrawl(_rootUri, 3);
        }

        private void RecursiveCrawl(Uri uri, ushort iterations)
        {
            if (iterations == 0)
            {
                return;
            }

            PageCrawler pageCrawler = new PageCrawler(uri);
            List<Product> pageProducts = pageCrawler.GetProducts();
            pageProducts.Sort((x, y) => x.Price.CompareTo(y.Price));

            _topProducts.UpdateList(pageProducts, _nrProducts);

            foreach (var pageUri in pageCrawler.GetUris())
            {
                bool reached = true;
                do
                {
                    try
                    {
                        if (!_visitedUris.Contains(pageUri))
                        {
                            Console.WriteLine(uri.ToString() + " - done");

                            _visitedUris.Add(pageUri);
                            RecursiveCrawl(pageUri, iterations--);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        Console.WriteLine("Trying again in 0.1 seconds.");
                        System.Threading.Thread.Sleep(100);
                        reached = false;
                    }
                } while (reached);
            }
        }
    }
}
