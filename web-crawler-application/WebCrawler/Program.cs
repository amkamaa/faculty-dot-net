﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WebCrawler
{
    class Program
    {
        static void Main(string[] args)
        {
            Uri website = new Uri("https://www.pcgarage.ro/");
            if (args.Length != 1)
            {
                Console.WriteLine("First argument should be a number!");
                Console.WriteLine("Usage: " + AppDomain.CurrentDomain.FriendlyName + " number");
                Environment.Exit(1);
            }

            try
            {
                var webCrawler = new WebCrawler(website, int.Parse(args[0]));
                webCrawler.CrawlPages();

                Console.WriteLine($"Top {args[0]} products are: \n{webCrawler.TopProducts}");
            } catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }
    }
}
