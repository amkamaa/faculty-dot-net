﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCrawler
{
    class SortedList
    {
        private List<Product> _products;

        public SortedList()
        {
            _products = new List<Product>();
        }

        public void UpdateList(List<Product> products, int size)
        {
            products.Sort();
            _products.Sort();
            _products = _products.Concat(products).ToList().GetRange(0, _products.Count < size ? _products.Count : size);
        }
    }
}
