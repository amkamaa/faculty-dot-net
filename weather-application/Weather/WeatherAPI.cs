﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Weather
{
    class WeatherApi
    {
        public static JsonObject Json = new JsonObject();

        public static async Task GetWeatherAsync(string cityName)
        {
            const string key = "fdc21fa0d7825d242d6329b477b26ef0";
            var httpClient = new HttpClient();
            var httpAsyncResponse = await httpClient.GetStringAsync(
                    $"http://api.openweathermap.org/data/2.5/weather?APPID={key}&q={cityName}");
            Json = JsonConvert.DeserializeObject<JsonObject>(httpAsyncResponse);
        }

        private static string FormatDateTime(int unixTimestamp)
        {
            var dt = UnixTimeStampToDateTime(unixTimestamp);
            var result = "";
            if (dt.Hour < 10)
            {
                result += "0";
            }

            result += dt.Hour + ":";

            if (dt.Minute < 10)
            {
                result += "0";
            }

            result += dt.Minute;

            return result;
        }

        public static string GetSunset()
        {
            return FormatDateTime(Json.Sys.Sunset);
        }

        public static string GetSunrise()
        {
            return FormatDateTime(Json.Sys.Sunrise);
        }

        public static double GetTemperature()
        {
            return ConvertKelvinToCelsius(Json.Main.Temp);
        }

        public static double GetClouds()
        {
            return Json.Clouds.All;
        }

        public static double GetHumidity()
        {
            return Json.Main.Humidity;
        }

        public static double GetWind()
        {
            return Json.Wind.Speed;
        }

        public static double GetPressure()
        {
            return Json.Main.Pressure;
        }

        private static double ConvertKelvinToCelsius(double kelvin)
        {
            return Math.Round(kelvin - 273.15, 2);
        }

        private static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
    }
}