﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Weather
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private WeatherApi weatherApi = new WeatherApi();

        public MainWindow()
        {
            InitializeComponent();
            HideImages();
        }

        private void HideImages()
        {
            ImgSunrise.Visibility = Visibility.Hidden;
            ImgSunset.Visibility = Visibility.Hidden;
            ImgClouds.Visibility = Visibility.Hidden;
            ImgDegrees.Visibility = Visibility.Hidden;
            ImgHumidity.Visibility = Visibility.Hidden;
            ImgWind.Visibility = Visibility.Hidden;
            ImgPressure.Visibility = Visibility.Hidden;
        }

        private void EraseLabels()
        {
            LblClouds.Content = "";
            LblHumidity.Content = "";
            LblPressure.Content = "";
            LblSunrise.Content = "";
            LblSunset.Content = "";
            LblTemperature.Content = "";
            LblWind.Content = "";
        }

        private void ShowImages()
        {
            ImgSunrise.Visibility = Visibility.Visible;
            ImgSunset.Visibility = Visibility.Visible;
            ImgClouds.Visibility = Visibility.Visible;
            ImgDegrees.Visibility = Visibility.Visible;
            ImgHumidity.Visibility = Visibility.Visible;
            ImgWind.Visibility = Visibility.Visible;
            ImgPressure.Visibility = Visibility.Visible;
        }

        private async void Send_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LblErrorCity.Content = "";
                await WeatherApi.GetWeatherAsync(TbCityName.Text);

                ShowImages();

                LblTemperature.Content = WeatherApi.GetTemperature() + "°C";
                LblClouds.Content = WeatherApi.GetClouds() + "%";
                LblHumidity.Content = WeatherApi.GetHumidity() + "%";
                LblWind.Content = WeatherApi.GetWind() + "m/s";
                LblPressure.Content = WeatherApi.GetPressure() + "hPa";
                LblSunset.Content = WeatherApi.GetSunset();
                LblSunrise.Content = WeatherApi.GetSunrise();
            }
            catch (Exception)
            {
                LblErrorCity.Content = $"City {TbCityName.Text} not found!";
                HideImages();
                EraseLabels();
            }
        }
    }
}
