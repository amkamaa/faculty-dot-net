﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Shapes;
using System.Threading;
using System.Configuration;
using Microsoft.CognitiveServices.SpeechRecognition;
using Microsoft.Azure.CognitiveServices.Language.TextAnalytics;
using Microsoft.Azure.CognitiveServices.Language.TextAnalytics.Models;

namespace SpeechRecognition
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private AutoResetEvent _finalResponseEvent;
        private MicrophoneRecognitionClient _microphoneRecognition;
        private ITextAnalyticsAPI _textAnalyticsClient;
        public MainWindow()
        {
            InitializeComponent();
            _textAnalyticsClient = new TextAnalyticsAPI
            {
                AzureRegion = AzureRegions.Westcentralus,
                SubscriptionKey = ConfigurationManager.AppSettings["MsTextAnalyticsApi"]
            };
            _finalResponseEvent = new AutoResetEvent(false);
            Ellipse.Visibility = Visibility.Hidden;
            LblRecording.Visibility = Visibility.Hidden;
            SentimentPercentLbl.Visibility = Visibility.Hidden;
            TextSentimentLbl.Visibility = Visibility.Hidden;
        }

        private void SpeechToText()
        {
            var speechRecognitionMode = SpeechRecognitionMode.ShortPhrase;
            var language = "en-us";
            var key = ConfigurationManager.AppSettings["MsSpeechApi"];
            _microphoneRecognition =
                SpeechRecognitionServiceFactory.CreateMicrophoneClient(speechRecognitionMode, language, key);
            _microphoneRecognition.OnPartialResponseReceived += ResponseReceivedHandler;
            _microphoneRecognition.StartMicAndRecognition();
        }

        private void ResponseReceivedHandler(object sender, PartialSpeechResponseEventArgs e)
        {
            string result = e.PartialResult;
            Dispatcher.Invoke(() =>
            {
                ResponseTb.Text = e.PartialResult;
                ResponseTb.Text += "\n";
            });
        }

        private void BtnStartRecord_Click(object sender, RoutedEventArgs e)
        {
            BtnStartRecord.IsEnabled = false;
            Ellipse.Visibility = Visibility.Visible;
            LblRecording.Visibility = Visibility.Visible;
            SentimentPercentLbl.Visibility = Visibility.Hidden;
            TextSentimentLbl.Visibility = Visibility.Hidden;
            SpeechToText();
        }

        private void BtnStopRecord_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke((() =>
            {
                _finalResponseEvent.Set();
                _microphoneRecognition.EndMicAndRecognition();
                _microphoneRecognition.Dispose();
                _microphoneRecognition = null;

                BtnStartRecord.IsEnabled = true;
                Ellipse.Visibility = Visibility.Hidden;
                LblRecording.Visibility = Visibility.Hidden;
                SentimentPercentLbl.Visibility = Visibility.Visible;
                TextSentimentLbl.Visibility = Visibility.Visible;
            }));

            SentimentBatchResult result3 = _textAnalyticsClient.Sentiment(
                new MultiLanguageBatchInput(
                    new List<MultiLanguageInput>()
                    {
                        new MultiLanguageInput("en", "0", ResponseTb.Text),
                    }));

            foreach (var document in result3.Documents)
            {
                Console.WriteLine("Document ID: {0} , Sentiment Score: {1:0.00}", document.Id, document.Score);
                SentimentPercentLbl.Content = Math.Round((double)document.Score * 100, 2) + "%";
            }
        }
    }
}